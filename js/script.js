window.addEventListener("load", myInit, true); 
function myInit(){
	setPhotoSizes();
	setTextResponsive();
	setProjSectionSize();
	chooseRndColor();
}

window.addEventListener("resize", myResize, true); 
function myResize(){
	setPhotoSizes();
	setTextResponsive();
	setProjSectionSize();
}

function setPhotoSizes() {
	document.getElementById('photo').style.marginBottom ="-"+(document.getElementById('photo').offsetWidth/2)+"px";
	document.getElementById('nav-row').style.marginBottom = (document.getElementById('photo').offsetWidth/1.5)+"px";
	document.getElementById("photo").style.borderWidth = ""+(document.getElementById('photo').offsetWidth*3.85)/100+"px";
}

function setTextResponsive(){
	titleFontSize=parseInt((""+window.getComputedStyle(document.getElementById("title")).fontSize).slice(0,-2));
	subtitleFontSize=parseInt((""+window.getComputedStyle(document.getElementById("subtitle")).fontSize).slice(0,-2));

	titleCardArray = document.getElementsByName("title-card");
	contentCardArray = document.getElementsByName("content-card");
	linkCardArray = document.getElementsByName("link-btn");

	for (var i = 0; i < titleCardArray.length; i++) {
		titleCardArray[i].style.fontSize = (titleFontSize+subtitleFontSize)/2+"px";
	}

	for (var i = 0; i < contentCardArray.length; i++) {
		contentCardArray[i].style.fontSize = (titleFontSize+subtitleFontSize)/5+"px";
	}

	for (var i = 0; i < linkCardArray.length; i++) {
		linkCardArray[i].style.fontSize = (titleFontSize+subtitleFontSize)/6+"px";
	}
}

function setProjSectionSize(){
	if (window.matchMedia("(min-width: 992px)").matches) {
		document.getElementById('team-proj').style.height = "auto";
		document.getElementById('pers-proj').style.height = "auto";		

		persHeight = document.getElementById('pers-proj').offsetHeight;
		teamHeight = document.getElementById('team-proj').offsetHeight;

		if(persHeight >= teamHeight){
			document.getElementById('team-proj').style.height = document.getElementById('pers-proj').offsetHeight+"px";
		}else{
			document.getElementById('pers-proj').style.height = document.getElementById('team-proj').offsetHeight+"px";
		}	
	} else {
		document.getElementById('team-proj').style.height = "auto";
		document.getElementById('pers-proj').style.height = "auto";		
	}	
}

function chooseRndColor(){
	let bgcolors = ['red','pink', 'purple', 'deep-purple', 'indigo', 'blue', 'light-blue', 'teal', 'green', 'cyan','orange','deep-orange']
	let fgcolors = ['red-text','pink-text', 'purple-text', 'deep-purple-text', 'indigo-text', 'blue-text', 'light-blue-text', 'teal-text', 'green-text', 'cyan-text','orange-text','deep-orange-text']
	let index = Math.floor(Math.random() * bgcolors.length);
	
	document.getElementById("nav-row").classList.remove('teal');
	document.getElementById("nav-row").classList.add(bgcolors[index]);

	btnArray = document.getElementsByName("link-btn");
	for (var i = 0; i < linkCardArray.length; i++) {
		btnArray[i].classList.add(bgcolors[index]);
	}

	document.getElementById("linkedin_sara").classList.remove('teal-text');
	document.getElementById("linkedin_sara").classList.add(fgcolors[index]);
}